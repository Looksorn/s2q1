import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  constructor() { }

  isPrime(num: number): boolean {
    for(var i = 2; i < num; i++)
      if(num % i === 0) return false;
    return num > 1;
  }

  isPerfectSquare(num: number): boolean{
    let s = Math.sqrt(num);
    return (s * s == num);
  }

  isFibonacci(num: number): boolean{
    return this.isPerfectSquare(5 * num * num + 4) || this.isPerfectSquare(5 * num * num - 4);
  }
  
}