export class Calculator {

  constructor(
    public type: string,
    public value?: number,
    public result?: boolean,
  ) {  }

}