import { Component } from '@angular/core';

import { Calculator } from '../calculator.model';
import { CalculatorService } from '../calculator.service';

@Component({
  selector: 'app-calculation-form',
  templateUrl: './calculation-form.component.html',
  styleUrls: ['./calculation-form.component.css']
})
export class CalculationFormComponent {

  types: string[] = ['isPrime', 'isFibonacci'];
  caculator: Calculator = new Calculator('isPrime');

  constructor(private calculatorService: CalculatorService) { }

  setValidValue(val: number): number {
    if (val < 0) {
      return 1;
    } else if (Math.round(val) !== val) {
      return Math.round(val);
    }
    return val;
  }

  calculate() {
    const value = this.caculator.value;
    if (value) {
      this.caculator.value = this.setValidValue(value);
      this.caculator.result = this.caculator.type === 'isPrime' ? 
        this.calculatorService.isPrime(this.caculator.value) : 
        this.calculatorService.isFibonacci(this.caculator.value);
    }
  }

}